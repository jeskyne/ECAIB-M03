/*
Online Java - IDE, Code Editor, Compiler

Online Java is a quick and easy tool that helps you to build, compile, test your programs online.
*/
import java.util.Scanner;

public class Activitat_02
{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Quin és el teu nombre favorit?");
        
        float FavouriteNumber = sc.nextFloat();
        
        System.out.println("Quin nombre odies més?");
        
        float HatedNumber = sc.nextFloat();
        
        float Suma = FavouriteNumber+HatedNumber;
        float Resta = FavouriteNumber-HatedNumber;
        float Multiply = FavouriteNumber*HatedNumber;
        float Division = FavouriteNumber / HatedNumber; 
        
        String SSuma = String.valueOf(Suma);
        String SResta = String.valueOf(Resta);
        String SMultiply = String.valueOf(Multiply);
        String SDivision = String.valueOf(Division);
        
        String Resultat = (SSuma+SResta+SMultiply+SDivision);
        
        System.out.println(Resultat); 
        
        
        
        
        
        
        
        //System.out.println("Welcome to Online IDE!! Happy Coding :)");
    }
}
