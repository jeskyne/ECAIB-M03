/*
Online Java - IDE, Code Editor, Compiler

Online Java is a quick and easy tool that helps you to build, compile, test your programs online.
*/

import java.util.Scanner;
  

public class Activitat_06
{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        
        
        System.out.println("Quant val la moto?");
        
        float PreuInicial = sc.nextFloat();
        
        System.out.println("Quin descompte té (valor numèric?");
        
        //Cóm podem permetre un input que sigui un float + símbol? 
        
        float Discount = sc.nextFloat();
        
        float SubTotaL = (PreuInicial)-Discount/100*PreuInicial;
        
        System.out.println("Quin IVA té?");
        
        int IVA = sc.nextInt();
        
        float TOTAL = SubTotaL*IVA/100+SubTotaL;
        
        
        
        System.out.println(TOTAL);
        
        
        
        
        
        
        
        
        
        
       
    }
}
