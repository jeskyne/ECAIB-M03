/*
Online Java - IDE, Code Editor, Compiler

Online Java is a quick and easy tool that helps you to build, compile, test your programs online.
*/

import java.util.Scanner;


public class Activitat_09
{
    public static void main(String[] args) {
        
        Scanner aa = new Scanner(System.in);
        
       System.out.println("Quants segons?");
        
       int Segons = aa.nextInt();
       
       int Hora = Segons/3600;
       
       int Minuts = (Segons/60)-(Hora*60);
       
       int SegonsCal = (Segons)-(Minuts*60)-(Hora*3600);
       
       System.out.println(Hora +"h " + Minuts + "m " + SegonsCal + "s ");
       
       
       
    }
}
