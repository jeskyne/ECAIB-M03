/*
Online Java - IDE, Code Editor, Compiler

Online Java is a quick and easy tool that helps you to build, compile, test your programs online.
*/
import java.util.Scanner;

public class Activitat_04
{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Calculem la mitjana de tres nombres reals, si us plau introdueix el primer:");
        
        float FirstNumber = sc.nextFloat();
        
        System.out.println("Genial! Ara el segon:");
        
        float SecondNumber = sc.nextFloat();
        
        System.out.println("I...l'últim:");
        
        float ThirdNumber = sc.nextFloat();
        
        float Resultat = (FirstNumber+SecondNumber+ThirdNumber/3);
        
        System.out.println("La mitjana és:"+ Resultat);
        
        
        
        
        
        
        
        //System.out.println("Welcome to Online IDE!! Happy Coding :)");
    }
}
