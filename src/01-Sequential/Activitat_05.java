
/*
Online Java - IDE, Code Editor, Compiler

Online Java is a quick and easy tool that helps you to build, compile, test your programs online.
*/

import java.util.Scanner;
  

public class Activitat_05
{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Anem a calcular l'àrea d'un triangle, si us plau introduieix la base");
        
        float Base = sc.nextFloat();
        
        System.out.println("Ara l'açada:");
        
        float Height = sc.nextFloat();
        
        float Area = Base*Height/2;
        
        
        System.out.println("L'àrea és:" + Area);
        
        
        
        
        
       // System.out.println("Welcome to Online IDE!! Happy Coding :)");
    }
}
