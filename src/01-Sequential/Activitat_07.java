/*
Online Java - IDE, Code Editor, Compiler

Online Java is a quick and easy tool that helps you to build, compile, test your programs online.
*/

import java.util.Scanner;
  

public class Activitat_07 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);


        System.out.println("Virtual cilindret welcomes you, please, enter the radius:");

        double Radius = sc.nextDouble();

        System.out.println("Now its height, please:");

        double Height = sc.nextDouble();

        double Volume = 3.14 * Math.pow(Radius, 2) * Height;

        System.out.println("This is the volume:  " + Volume);

        double AreaLateral = 2 * 3.14 * Radius * Height;

        System.out.println("This is the area:  " + AreaLateral);


    }
}