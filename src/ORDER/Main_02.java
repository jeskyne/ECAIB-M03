import java.io.*;
import java.util.*;


public class Main_02

{
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        String str1 = sc.nextLine();
        String[] Parts = str1.split(" ");

        String Str1X = Parts[0];
        String Str1Y = Parts[1];

        int X1 = Integer.parseInt(Str1X);
        int Y1 = Integer.parseInt(Str1Y);


        String str2 = sc.nextLine();
        String[] Parts2 = str2.split(" ");

        String Str2X = Parts2[0];
        String Str2Y = Parts2[1];

        int X2 = Integer.parseInt(Str2X);
        int Y2 = Integer.parseInt(Str2Y);

        String str3 = sc.nextLine();
        String[] Parts3 = str3.split(" ");

        String Str3X = Parts3[0];
        String Str3Y = Parts3[1];

        int X3 = Integer.parseInt(Str3X);
        int Y3 = Integer.parseInt(Str3Y);

        String str4 = sc.nextLine();
        String[] Parts4 = str4.split(" ");

        String Str4X = Parts4[0];
        String Str4Y = Parts4[1];

        int X4 = Integer.parseInt(Str4X);
        int Y4 = Integer.parseInt(Str4Y);


        double DIS1 = Math.sqrt(Math.pow(X1,2) + Math.pow(Y1,2));

        double DIS2 = Math.sqrt(Math.pow(X1-X2,2) + Math.pow(Y1-Y2,2));

        double DIS3 = Math.sqrt(Math.pow(X2-X3,2) + Math.pow(Y2-Y3,2));

        double DIS4 = Math.sqrt(Math.pow((X3)-X4,2) + Math.pow(Y3-Y4,2));

        double DIS5 = Math.sqrt(Math.pow(X4,2) + Math.pow(Y4,2));

        double TOTALDIS = (DIS1+DIS2+DIS3+DIS4+DIS5);


        System.out.println("DIS1 " + DIS1);
        System.out.println("DIS2 " + DIS2);
        System.out.println("DIS3 " + DIS3);
        System.out.println("DIS4 " + DIS4);
        System.out.println("DIS5 " + DIS5);
        System.out.println(TOTALDIS);





    }
}
