import java.io.*;
import java.util.*;

import java.lang.Math;


public class Main2

{

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        //Double DIS0 = null;
        //Double DIS1 = null;
        Double DIS2 = null;
        Double DIS3 = null;
        Double DIS4 = null;
        //Double DIS5 = null;

        String str1 = sc.nextLine();
        String[] Parts = str1.split(" ");

        String Str1X = Parts[0];
        String Str1Y = Parts[1];

        int X1 = Integer.parseInt(Str1X);
        int Y1 = Integer.parseInt(Str1Y);


        String str2 = sc.nextLine();
        String[] Parts2 = str2.split(" ");

        String Str2X = Parts2[0];
        String Str2Y = Parts2[1];

        int X2 = Integer.parseInt(Str2X);
        int Y2 = Integer.parseInt(Str2Y);

       String str3 = sc.nextLine();
        String[] Parts3 = str3.split(" ");

        String Str3X = Parts3[0];
        String Str3Y = Parts3[1];

        int X3 = Integer.parseInt(Str3X);
        int Y3 = Integer.parseInt(Str3Y);

        String str4 = sc.nextLine();
        String[] Parts4 = str4.split(" ");

        String Str4X = Parts4[0];
        String Str4Y = Parts4[1];

        int X4 = Integer.parseInt(Str4X);
        int Y4 = Integer.parseInt(Str4Y);


        Double DIS0 = Math.sqrt(Math.pow(X1,2) + Math.pow(Y1,2));


        if (Y1==Y2 && X1==X2) DIS2 = 0.0;

        else if (Y1==Y2) {

            DIS2 = Double.valueOf(Math.abs(X1)) + Double.valueOf(Math.abs(X2));

        }

        else if (X1==X2) {

            DIS2 = Double.valueOf(Math.abs(Y1)) + Double.valueOf(Math.abs(Y2));
        }

        else {

            DIS2 = Math.sqrt(Math.pow(X1-X2,2) + Math.pow(Y1-Y2,2));

        }


        if (Y2==Y3 && X2==X3) DIS3 = 0.0;

        else if (Y2==Y3) {

            DIS3 = Double.valueOf(Math.abs(X2)) + Double.valueOf(Math.abs(X3));

        }

        else if (X2==X3) {

            DIS3 = Double.valueOf(Math.abs(Y2)) + Double.valueOf(Math.abs(Y3));
        }

        else {


            DIS3 = Math.sqrt(Math.pow(X2-X3,2) + Math.pow(Y2-Y3,2));
        }

        if (Y3==Y4 && X3==X4) DIS4 = 0.0;

        else if (Y3==Y4) {

            DIS4 = Double.valueOf(Math.abs(X3)) + Double.valueOf(Math.abs(X4));

        }

        else if (X3==X4) {

            DIS4 = Double.valueOf(Math.abs(Y3)) + Double.valueOf(Math.abs(Y4));
        }

        else {

            DIS4 = Math.sqrt(Math.pow((X3)-X4,2) + Math.pow(Y3-Y4,2));

        }



        double DIS5 = Math.sqrt(Math.pow(X4,2) + Math.pow(Y4,2));

        int TOTAL = (int)Math.round(DIS0+DIS4+DIS2+DIS3+DIS5);

        System.out.println(TOTAL);

        System.out.println("Distància #0: " + DIS0);
        System.out.println("Distància #2: " + DIS2);
        System.out.println("Distància #3: " + DIS3);
        System.out.println("Distància #4: " + DIS4);
        System.out.println("Distància #5: " + DIS5);

        }






















    }

